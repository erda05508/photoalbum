import random
from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now
from django.core.urlresolvers import reverse
from django.conf import settings
from .managers import AlbumQuerySet, PhotoQuerySet
from django.db.models import permalink

LATEST_LIMIT = getattr(settings, 'GALLERY_ALBUM_LATEST_LIMIT', None)
SAMPLE_SIZE = getattr(settings, 'GALLERY_ALBUM_SAMPLE_SIZE', 5)



class Album(models.Model):
    user = models.ForeignKey(User)
    title = models.CharField(max_length=200, blank=False, verbose_name='Названия')
    description = models.TextField(verbose_name='Описания')
    created_d = models.DateTimeField(default=now)
    mod_d = models.DateTimeField(auto_now_add=False, auto_now=True)
    is_public = models.BooleanField(default=True)
    objects = AlbumQuerySet.as_manager()
    photos = models.ManyToManyField('Photo', related_name='photos', null=True, blank=True)


    class Meta:
        ordering = ["mod_d"]
        get_latest_by = 'created_d'

    def __str__(self):
        return "Альбом: %s, User: %s" % (self.title, self.user)
   
    def get_absolute_url(self):
        return reverse('gallery:pl-gallery', kwargs={'pk': self.pk})
 
    def latest(self, limit=LATEST_LIMIT, public=True):
        if not limit:
            limit = self.photo_count()
        if public:
            return self.public()[:limit]
        else:
            return self.photos.all()[:limit]

    def sample(self, count=None, public=True):
        if not count:
            count = SAMPLE_SIZE
        if count > self.photo_count():
            count = self.photo_count()
        if public:
            photo_set = self.public()
        else:
            photo_set = self.photos.filter(sites__id=settings.SITE_ID)
        return random.sample(set(photo_set), count)

    def photo_count(self, public=True):
        if public:
            return self.public().count()
        else:
            return self.photos.all().count()
    photo_count.short_description = 'count'

    def public(self):
        return self.photos.filter(is_public=True)

 

class Photo(models.Model):
    title = models.CharField(max_length=200, blank=False)    
    created_d = models.DateTimeField(default=now)
    mod_d = models.DateTimeField(auto_now_add=False, auto_now=True)
    description = models.CharField(max_length=200, blank=True)
    is_public = models.BooleanField(default=True)
    objects = PhotoQuerySet.as_manager()

    image = models.ImageField(upload_to='mediafiles/o/%Y-%m-%d')

    class Meta:
        ordering = ["mod_d"]
  
    def __str__(self):
        return self.title
 
    def save(self, *args, **kwargs):
        if self.pk is None:
            self.pk = self.id
        super(Photo, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('gallery:pl-photo', args=[self.id])

