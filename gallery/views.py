from django.views.generic.dates import ArchiveIndexView, DateDetailView, DayArchiveView, MonthArchiveView, YearArchiveView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.base import RedirectView
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from .models import Album, Photo
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from .forms import AlbumForm, PhotoForm



def index(request):
    queryset = Album.objects.filter(is_public=True)
    return render(request, 'gallery/index.html')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/accounts/login/')
    else:
        form = UserCreationForm()

        args = {'form': form}
        return render(request, 'registration/reg_form.html', args)


class AlbumList(ListView):
    queryset = Album.objects.filter(is_public=True)


class AlbumDetail(DetailView):
    queryset = Album.objects.filter(is_public=True)



class AlbumDateDetail(DateDetailView):
    queryset = Album.objects.filter(is_public=True)
    date_field = 'created_d'
    allow_future = True


class AlbumArchiveIndexView(ArchiveIndexView):
    queryset = Album.objects.filter(is_public=True)
    date_field = 'created_d'
    allow_future = True


class AlbumDayArchiveView(DayArchiveView):
    queryset = Album.objects.filter(is_public=True)
    date_field = 'created_d'
    allow_future = True


class AlbumMonthArchiveView(MonthArchiveView):
    queryset = Album.objects.filter(is_public=True)
    date_field = 'created_d'
    allow_future = True


class AlbumYearArchiveView(YearArchiveView):
    queryset = Album.objects.filter(is_public=True)
    date_field = 'created_d'
    make_object_list = True
    allow_future = True

@login_required
def album_new(request):
    if request.method == "POST":
        form = AlbumForm(request.POST)
        if form.is_valid():
            album = form.save(commit=False)
            album.author = request.user
            album.save()
            return redirect('gallery:pl-gallery', pk=album.pk)
    else:
        form = AlbumForm()
    return render(request, 'gallery/album_edit.html', {'form': form})


class AlbumUpdate(UpdateView):
    model = Album
    fields = ['user', 'title', 'description', 'photos']
    template_name = 'gallery/album_update_form.html'  

class AlbumDelete(DeleteView):
    model = Album
    success_url = reverse_lazy('gallery:index')



class PhotoList(ListView):
    queryset = Photo.objects.filter(is_public=True)


class PhotoDetail(DetailView):
    queryset = Photo.objects.filter(is_public=True)


class PhotoDateDetail(DateDetailView):
    queryset = Photo.objects.filter(is_public=True)
    date_field = 'created_d'
    allow_future = True


class PhotoArchiveIndexView(ArchiveIndexView):
    queryset = Photo.objects.filter(is_public=True)
    date_field = 'created_d'
    allow_future = True


class PhotoDayArchiveView(DayArchiveView):
    queryset = Photo.objects.filter(is_public=True)
    date_field = 'created_d'
    allow_future = True


class PhotoMonthArchiveView(MonthArchiveView):
    queryset = Photo.objects.filter(is_public=True)
    date_field = 'created_d'
    allow_future = True


class PhotoYearArchiveView(YearArchiveView):
    queryset = Photo.objects.filter(is_public=True)
    date_field = 'created_d'
    make_object_list = True
    allow_future = True

class PhotoAdd(CreateView):
    model = Photo
    fields = ['image', 'title', 'description']

class PhotoUpdate(UpdateView):
    model = Photo
    fields = ['image', 'title', 'description']
    template_name = 'gallery/photo_update_form.html'

class PhotoDelete(DeleteView):
    model = Photo
    success_url = reverse_lazy('gallery:index')




