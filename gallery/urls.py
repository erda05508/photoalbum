from django.conf.urls import url
from . import views
from django.contrib.auth.views import login, logout
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', login, {'template_name': 'registration/login.html'}),
    url(r'^logout/$', logout, {'template_name': 'registration/logged_out.html'}),
    url(r'^register/$', views.register, name='register'),
    url(r'^album/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/(?P<pk>[0-9]+)/$', views.AlbumDateDetail.as_view(month_format='%m'), name='album-detail'),
    url(r'^album/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/$', views.AlbumDayArchiveView.as_view(month_format='%m'), name='gallery-archive-day'),
    url(r'^album/(?P<year>\d{4})/(?P<month>[0-9]{2})/$', views.AlbumMonthArchiveView.as_view(month_format='%m'), name='gallery-archive-month'),
    url(r'^album/(?P<year>\d{4})/$', views.AlbumYearArchiveView.as_view(), name='pl-gallery-archive-year'),
    url(r'^album/$', views.AlbumArchiveIndexView.as_view(), name='pl-gallery-archive'),
    url(r'^album/(?P<pk>[0-9]+)/$', views.AlbumDetail.as_view(), name='pl-gallery'),
    url(r'^albumlist/$', views.AlbumList.as_view(), name='album-list'), 
    url(r'^album/new/$', views.album_new, name='album_new'),
    url(r'^album/change/(?P<pk>[0-9]+)/$', views.AlbumUpdate.as_view(), name='album_update'),
    url(r'^album/delete/(?P<pk>[0-9]+)/$', views.AlbumDelete.as_view(), name='album_delete'),
    
    url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/(?P<pk>[0-9]+)/$', views.PhotoDateDetail.as_view(month_format='%m'), name='photo-detail'),
    url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/(?P<day>\w{1,2})/$', views.PhotoDayArchiveView.as_view(month_format='%m'), name='photo-archive-day'),
    url(r'^photo/(?P<year>\d{4})/(?P<month>[0-9]{2})/$', views.PhotoMonthArchiveView.as_view(month_format='%m'), name='photo-archive-month'),
    url(r'^photo/(?P<year>\d{4})/$', views.PhotoYearArchiveView.as_view(), name='pl-photo-archive-year'),
    url(r'^photo/$', views.PhotoArchiveIndexView.as_view(), name='pl-photo-archive'),
    url(r'^photo/(?P<pk>[0-9]+)/$', views.PhotoDetail.as_view(), name='pl-photo'),
    url(r'^photolist/$', views.PhotoList.as_view(), name='photo-list'),
    url(r'^photo/new/$', views.PhotoAdd.as_view(), name='photo_new'),
    url(r'^photo/change/(?P<pk>[0-9]+)/$', views.PhotoUpdate.as_view(), name='photo_change'),
    url(r'^photo/delete/(?P<pk>[0-9]+)/$', views.PhotoDelete.as_view(), name='photo_delete'),

    
]
