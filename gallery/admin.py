from django.contrib import admin
from .models import Album, Photo



class AlbumAdmin(admin.ModelAdmin):
    list_display = ['title', 'user', 'description', 'created_d']
    list_filter = ['title', 'user']
   
 
    class Meta:
        model = Album

admin.site.register(Album, AlbumAdmin)

class PhotoAdmin(admin.ModelAdmin):
    class Meta:
        model = Photo

admin.site.register(Photo, PhotoAdmin)

