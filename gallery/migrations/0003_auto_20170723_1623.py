# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-23 16:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0002_auto_20170720_1806'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='photo',
            name='album',
        ),
        migrations.AddField(
            model_name='album',
            name='photos',
            field=models.ManyToManyField(blank=True, null=True, to='gallery.Photo'),
        ),
    ]
