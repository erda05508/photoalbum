from django import forms
from .models import Album, Photo


class AlbumForm(forms.ModelForm):
    class Meta: 
        model = Album
        fields = ['user', 'title', 'description', 'photos']
        

class PhotoForm(forms.ModelForm):
    class Meta: 
        model = Photo
        fields = ['image', 'title', 'description']



